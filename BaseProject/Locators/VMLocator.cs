﻿using BaseProject.ViewModels;
using Funq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseProject.Locators
{
    public class VMLocator
    {
        private static readonly Container container = new Container();

        public static MainViewModel Main
        {
            get
            {
                return container.Resolve<MainViewModel>();
            }
        }

        public VMLocator()
        {
            container.Register<MainViewModel>(c => new MainViewModel());
        }
    }
}
