﻿using BaseProject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BaseProject.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields
        #endregion

        #region Properteis
        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                base.RaisePropertyChanged(() => Title);
            }
        }
        #endregion

        #region Commands
        RelayCommand _exampleCommand;
        public ICommand ExampleCommand
        {
            get
            {
                if (_exampleCommand == null)
                {
                    _exampleCommand = new RelayCommand(ExampleCommandExecute);
                }
                return _exampleCommand;
            }
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            Title = "Base Project";
        }
        #endregion

        #region Methods
        private void ExampleCommandExecute(object obj)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
