# README #

This project is a base project to start any other MVVM project from. 

### What does this project include? ###

* Uses Funq for ViewModelLocator
* App.xaml already setup with ViewModelLocator
* Comes with a MainView already binded to a MainViewModel
* RelayCommand Class
    * Example Relay in MainViewModel
* BaseViewModel class already setup for easy binding
* Title already bound to a property inside of MainViewModel

### How do I get set up? ###

* Fork this repository
* Change the name of the solution to the name of your project (use Visual Studio for this)
* Change the name of the project to the name of your project (use Visual Studio for this)
* Use Visual Studio Find and Replace to replace every instance of "BaseProject" to "<Your Project Name>"
* Compile... Run... Have fun!